# Gotify Desktop Notification Daemon

## Summary

Connects to a gotify instance and creates desktop notifications for incoming
messages.

## Build

`$ git clone https://gitlab.com/cosmo_duff/gotify-desktop-notification-daemon &&
cd gotify-desktop-notification-daemon`

`$ cargo build`

## Install

`$ git clone https://gitlab.com/cosmo_duff/gotify-desktop-notification-daemon &&
cd gotify-desktop-notification-daemon`

`$ cargo install --path ./`

## Usage
```
Gotify Desktop Notification Daemon 0.1.0
Receive notifications from gotify on your desktop.

USAGE:
    gdnd [FLAGS] [OPTIONS] --url <url>

FLAGS:
    -F, --foreground    Run GDND in the foreground
    -h, --help          Prints help information
    -V, --version       Prints version information

OPTIONS:
    -c, --client <client>        Gotify client name: Required if authenticating with username and password
    -p, --password <password>    Gotify password [env: GDND_PASSWORD]
    -P, --poll <poll>            Time between polling the gotify server in seconds [default: 1]
    -t, --token <token>          Gotify client token: If used username password and client are not needed [env:
                                 GDND_TOKEN]
    -u, --url <url>              Gotify server url
    -U, --username <username>    Gotify username [env: GDND_USER]
```

## Authentication

Authentication can be done by either using a pre-created client token or by
connecting using username and password. When using a token the values for
username, password and client are not required and will be ignored. If authentication is done using
username and password after the first connection the client is cached and only
the value for client is required.

## Examples
The URL https://instance.tld will be used for all examples.

### Using a token

`gdnd -u https://instance.tld -t clienttoken`

The token can be set via an environment variable.

```
$ export GDND_TOKEN=clienttoken

$ gdnd -u https://instance.tld
```

### Using Username and Password

`$ gdnd -u https://instance.tld -U username -p password -c client_name`

The username and password can be set as environment variables.

```
$ export GDND_USER=username
$ export GDND_PASSWORD=password

$ gdnd -u https://instance.tld -c client_name
```

The client token is cached and the username and password are not necessary for subsequent connections. The cache location is $HOME/.cache/gdnd/\<client_name\>.json.

`$ gdnd -u https://instance.tld -c client_name`
