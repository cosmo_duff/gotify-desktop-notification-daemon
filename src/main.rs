pub mod args;
pub mod errors;
pub mod gdnd_client;
pub mod gotify;
pub mod helpers;
pub mod userauth;

use std::convert::TryFrom;

use crate::{
    args::Args,
    errors::GdndError,
    gdnd_client::GdndClient,
    helpers::{base_url, to_websocket},
    userauth::UserAuth,
};
use log::{error, info, warn};
use simplelog::{Config, LevelFilter, SimpleLogger};
use structopt::StructOpt;

fn main() {
    // this should never fail but if it does we will write to stderr instead of trying to rely on logging for obvious
    // reasons
    // TO DO: configurable log levels
    if let Err(e) = SimpleLogger::init(LevelFilter::Info, Config::default()) {
        eprintln!("Failed to initiate the logger: {}", e);
        std::process::exit(1);
    };

    // mutable to update from env vars
    let args = Args::from_args();

    if let Err(e) = run(args) {
        error!("{}", e);
        std::process::exit(1);
    }
}

fn run(args: Args) -> Result<(), GdndError> {
    // make sure the URL is clean
    let url = base_url(&args.url)?;
    let ws_url = to_websocket(url.clone())?;

    let poll = args.poll;

    if !args.foreground {
        // daemonize the  process
        let daemonize = daemonize::Daemonize::new();
        daemonize.start()?;
    }

    // create the gdnd client
    // try token auth first
    let gdnd_cli = if let Some(t) = args.token {
        GdndClient::new(ws_url, t, None)
    } else if args.client.is_some() {
        // this unwrap cant fail since we know it is some
        let cli_name = args.client.clone().unwrap();
        // try to read from the cache
        match GdndClient::from_cache(&cli_name) {
            Ok(gc) => gc,
            Err(_) => {
                // attempt user auth
                let user_auth = UserAuth::try_from(args)?;
                let gc = GdndClient::from_user_auth(user_auth)?;
                // write the client to cache if auth was successful
                match gc.write_cache() {
                    Ok(_) => info!("Wrote cahce."),
                    Err(e) => warn!("Failed to write client cache: {}", e),
                }

                gc
            }
        }
    } else {
        let err_msg = "No authentication or connection method is possible.".to_string();
        return Err(GdndError::MissingArgs(err_msg));
    };

    gdnd_cli.run_loop(poll)
}
